// Use an object literal: {} to create an object representing a user
	// Encapsulation
	// Whenever we add properties or methods to an object, we are performing ENCAPSULATION
	// The organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
	// The scope of encapsulation is denoted by object literals

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

// Methods
	// Add the functionalities available to a student as object methods
	// The keyword 'this' refers to the object encapsulating the method where 'this' is called

	login() {
		console.log(`${this.email} has logged in.`);
	},

	logout() {
		console.log(`${this.email} has logged out.`);
	},
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
	},

	// Mini-activity 1
		// Create a function that will get the average of studentOne's grades
	getAverage() {
		totalGrade = this.grades.reduce((acc, curr) => acc + curr)
		return totalGrade / this.grades.length
	},

	// Mini-activity 2
		// Create a function that will return true if average grade is >= 85, false otherwise

	isPassed() {
		if (this.getAverage() >= 85) {
			return true
		} else {
			return false
		}
	},

	// Mini-activity 3
		// Create a function called willPassWithHonors() that returns true if the student has passed and their average is >= 90. The function returns false if either one is not met

	willPassWithHonors() {
		if (this.isPassed() === true && this.getAverage() >= 90) {
			return true
		} else {
			return false
		}
	}

}

// Log the content of studentOne's encapsulated information in the console
console.log(`Student One's name is ${studentOne.name}`);
console.log(`Student One's email is ${studentOne.email}`);
console.log(`Student One's quarterly grade averages are ${studentOne.grades}`);