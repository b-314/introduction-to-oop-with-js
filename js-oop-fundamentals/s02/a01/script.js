/*
1. What is the term given to unorganized code that's very hard to work with?

Spaghetti Code

2. How are object literals written in JS?

Curly brackets ({})

3. What do you call the concept of organizing information and functionality to belong to an object?

Encapsulation

4. If studentOne has a method named enroll(), how would you invoke it?

studentOne.enroll()

5. True or False: Objects can have objects as properties.

True

6. What is the syntax in creating key-value pairs?

key: value - during instantiation
-or-
dot notation
-or-
square bracket notation

7. True or False: A method can have no parameters and still work.

True

8. True or False: Arrays can have objects as elements.

True

9. True or False: Arrays are objects.

True

10. True or False: Objects can have arrays as properties.

True

*/

//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail.
//For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90,
//false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//create student one
let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],
	getAverage() {
		totalGrade = this.grades.reduce((acc, curr) => acc + curr)
		return totalGrade / this.grades.length
	},
	willPass() {
		if (this.getAverage() >= 85) {
			return true
		} else {
			return false
		}
	},
	willPassWithHonors() {
		if (this.getAverage() >= 90) {
			return true
		} else if (this.getAverage() < 90 && this.getAverage() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}

//create student two
let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],
	getAverage() {
		totalGrade = this.grades.reduce((acc, curr) => acc + curr)
		return totalGrade / this.grades.length
	},
	willPass() {
		if (this.getAverage() >= 85) {
			return true
		} else {
			return false
		}
	},
	willPassWithHonors() {
		if (this.getAverage() >= 90) {
			return true
		} else if (this.getAverage() < 90 && this.getAverage() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}

//create student three
let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],
	getAverage() {
		totalGrade = this.grades.reduce((acc, curr) => acc + curr)
		return totalGrade / this.grades.length
	},
	willPass() {
		if (this.getAverage() >= 85) {
			return true
		} else {
			return false
		}
	},
	willPassWithHonors() {
		if (this.getAverage() >= 90) {
			return true
		} else if (this.getAverage() < 90 && this.getAverage() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}

//create student four
let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],
	getAverage() {
		totalGrade = this.grades.reduce((acc, curr) => acc + curr)
		return totalGrade / this.grades.length
	},
	willPass() {
		if (this.getAverage() >= 85) {
			return true
		} else {
			return false
		}
	},
	willPassWithHonors() {
		if (this.getAverage() >= 90) {
			return true
		} else if (this.getAverage() < 90 && this.getAverage() >= 85) {
			return false
		} else {
			return undefined
		}
	}
}


//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo()
//that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc()
//that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents() {
		let noOfHonorStudents = 0;
		this.students.forEach((student) => {
			if (student.willPassWithHonors() === true) {
				noOfHonorStudents++
			}
		})
		return noOfHonorStudents
	},
	honorsPercentage() {
		return (this.countHonorStudents() / this.students.length) * 100
	},
	retrieveHonorStudentInfo() {
		let honorStudentInfoArray = [];
		this.students.forEach((student) => {
			if (student.willPassWithHonors() === true) {
				honorStudentInfoArray.push(student)
			}
		})
		return honorStudentInfoArray
	},
	sortHonorStudentsByGradeDesc() {
		return this.retrieveHonorStudentInfo().sort((a, b) => {
			return (b.getAverage() - a.getAverage())
		})
	}
}